package com.tcwgq.learnmybatisplusspringboot.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tcwgq.learnmybatisplusspringboot.enums.SexEnum;
import com.tcwgq.learnmybatisplusspringboot.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author tcwgq
 * @since 2023/7/11 21:57
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class UserMapperTest {
    @Resource
    private UserMapper userMapper;

    @Test
    public void findAll() {
        List<User> users = userMapper.selectList(null);
        for (User user : users) {
            System.out.println(user);
        }
    }

    @Test
    public void insert() {
        User user = new User();
        // user.setId();
        user.setUserName("aaa");
        // user.setPassword("bbb");
        user.setName("aaa");
        user.setAge(27);
        user.setMail("aaa@163.com");
        user.setSex(SexEnum.MAN);
        int row = userMapper.insert(user);
        System.out.println("插入用户结果row=" + row);
    }

    @Test
    public void testTableField() {
        User user = new User();
        // user.setId();
        user.setUserName("aaa1");
        user.setPassword("bbb1");
        user.setName("aaa1");
        user.setAge(271);
        user.setMail("aaa@163.com1");
        user.setAddress("北京市昌平区");
        int row = userMapper.insert(user);
        System.out.println("插入用户结果row=" + row);
    }

    @Test
    public void updateById() {
        User user = new User();
        user.setId(1L);
        user.setUserName("aaa1");
        user.setPassword("bbb1");
        user.setName("aaa1");
        user.setAge(271);
        user.setMail("aaa@163.com1");
        user.setAddress("北京市昌平区");
        int row = userMapper.updateById(user);
        System.out.println("更新用户结果row=" + row);
    }

    @Test
    public void update() {
        User user = new User();
        user.setUserName("aaa1");
        user.setPassword("bbb1");
        user.setName("aaa1");
        user.setAge(271);
        user.setMail("aaa@163.com1");
        user.setAddress("北京市昌平区");
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("user_name", "lisi");
        int row = userMapper.update(user, wrapper);
        System.out.println("更新用户结果row=" + row);
    }

    @Test
    public void update1() {
        UpdateWrapper<User> wrapper = new UpdateWrapper<>();
        wrapper.set("age", 32).set("password", "123456"); // 要设置的值
        wrapper.eq("user_name", "aaa1");// 更新条件
        int row = userMapper.update(null, wrapper);
        System.out.println("更新用户结果row=" + row);
    }

    @Test
    public void deleteById() {
        int row = userMapper.deleteById(3L);
        System.out.println("删除用户结果row=" + row);
    }

    @Test
    public void deleteByMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("user_name", "zhangSan");
        map.put("password", "123456"); // 多个是and条件
        int row = userMapper.deleteByMap(map);
        System.out.println("删除用户结果row=" + row);
    }

    @Test
    public void delete() {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("user_name", "lisi")
                .eq("password", "123456");
        int row = userMapper.delete(wrapper);
        System.out.println("删除用户结果row=" + row);
    }

    @Test
    public void delete1() {
        // 推荐使用这种方式
        User user = new User();
        user.setUserName("aaa1");
        user.setPassword("bbb1");
        QueryWrapper<User> wrapper = new QueryWrapper<>(user);
        int row = userMapper.delete(wrapper);
        System.out.println("删除用户结果row=" + row);
    }

    @Test
    public void deleteBatchIds() {
        // 推荐使用这种方式
        int row = userMapper.deleteBatchIds(Arrays.asList(1L, 2L));
        System.out.println("删除用户结果row=" + row);
    }


    @Test
    public void selectById() {
        User user = userMapper.selectById(3L);
        System.out.println(user);
    }

    @Test
    public void selectBatchIds() {
        List<User> users = userMapper.selectBatchIds(Arrays.asList(3L, 4L));
        System.out.println(users);
    }

    @Test
    public void selectOne() {
        // 查出结果多条报错
        User user = new User();
        user.setUserName("aaa1");
        user.setPassword("bbb1");
        QueryWrapper<User> wrapper = new QueryWrapper<>(user);
        User result = userMapper.selectOne(wrapper);
        System.out.println(result);
    }

    @Test
    public void selectCount() {
        // 查出结果多条报错
        User user = new User();
        user.setUserName("aaa1");
        user.setPassword("bbb1");
        QueryWrapper<User> wrapper = new QueryWrapper<>(user);
        wrapper.gt("age", 20);
        Integer count = userMapper.selectCount(wrapper);
        System.out.println(count);
    }

    @Test
    public void selectList() {
        // 查出结果多条报错
        User user = new User();
        user.setUserName("aaa1");
        user.setPassword("bbb1");
        QueryWrapper<User> wrapper = new QueryWrapper<>(user);
        wrapper.like("email", "aaa");
        List<User> users = userMapper.selectList(wrapper);
        for (User user1 : users) {
            System.out.println(user1);
        }
    }

    @Test
    public void selectPage() {
        // 查出结果多条报错
        User user = new User();
        IPage<User> iPage = new Page<>(2, 2);
        QueryWrapper<User> wrapper = new QueryWrapper<>(user);
        wrapper.like("email", "test");
        IPage<User> page = userMapper.selectPage(iPage, wrapper);
        System.out.println("pages=" + page.getPages());
        System.out.println("total=" + page.getTotal());
        System.out.println("records" + page.getRecords());
    }

    @Test
    public void findById() {
        User user = userMapper.findById(3L);
        System.out.println(user);
    }

    @Test
    public void deleteAll() {
        // 删除所有操作被SqlExplainInterceptor拦截，Prohibition of full table deletion
        int delete = userMapper.delete(null);
        System.out.println(delete);
    }

    /**
     * 乐观锁机制
     * <p>
     * 仅支持 updateById(id) 与 update(entity, wrapper) 方法
     * 在 update(entity, wrapper) 方法下, wrapper 不能复用!!!
     */
    @Test
    public void testUpdate() {
        User user = new User();
        user.setId(3L);
        user.setAge(30);
        user.setVersion(1); // 获取到version为1
        int result = userMapper.updateById(user);
        System.out.println("result = " + result);
    }

    @Test
    public void testFindAll() {
        List<User> all = userMapper.findAll();
        System.out.println("result = " + all);
    }

}