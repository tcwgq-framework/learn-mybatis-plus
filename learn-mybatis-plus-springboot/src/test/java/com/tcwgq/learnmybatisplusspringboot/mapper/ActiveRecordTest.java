package com.tcwgq.learnmybatisplusspringboot.mapper;

import com.tcwgq.learnmybatisplusspringboot.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author tcwgq
 * @since 2023/7/11 21:57
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class ActiveRecordTest {
    @Test
    public void selectById() {
        User user = new User();
        user.setId(2L);
        User user2 = user.selectById();
        System.out.println(user2);
    }

    @Test
    public void insert() {
        User user = new User();
        user.setName("刘备");
        user.setAge(30);
        user.setPassword("123456");
        user.setUserName("liubei");
        user.setMail("liubei@itcast.cn");
        boolean insert = user.insert();
        System.out.println(insert);
    }


    @Test
    public void updateById() {
        User user = new User();
        user.setId(8L);
        user.setAge(35);
        boolean update = user.updateById();
        System.out.println(update);
    }

    @Test
    public void deleteById() {
        User user = new User();
        user.setId(7L);
        boolean delete = user.deleteById();
        System.out.println(delete);
    }

}