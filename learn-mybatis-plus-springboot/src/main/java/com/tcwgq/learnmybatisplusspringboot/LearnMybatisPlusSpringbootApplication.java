package com.tcwgq.learnmybatisplusspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LearnMybatisPlusSpringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(LearnMybatisPlusSpringbootApplication.class, args);
    }

}
