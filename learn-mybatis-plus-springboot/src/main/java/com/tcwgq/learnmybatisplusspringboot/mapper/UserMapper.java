package com.tcwgq.learnmybatisplusspringboot.mapper;

import com.tcwgq.learnmybatisplusspringboot.model.User;

/**
 * @author tcwgq
 * @since 2023/7/9 21:50
 */
public interface UserMapper extends MyBaseMapper<User> {
    User findById(Long id);

}