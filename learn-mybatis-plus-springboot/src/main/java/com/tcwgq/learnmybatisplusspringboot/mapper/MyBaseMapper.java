package com.tcwgq.learnmybatisplusspringboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * @author tcwgq
 * @since 2023/7/16 17:47
 */
public interface MyBaseMapper<T> extends BaseMapper<T> {
    List<T> findAll();

}
