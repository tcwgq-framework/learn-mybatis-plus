package com.tcwgq.learnmybatisplusspringboot.model;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.tcwgq.learnmybatisplusspringboot.enums.SexEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author tcwgq
 * @since 2023/7/9 21:50
 */
@Data
// @TableName("tb_user")
@NoArgsConstructor
@AllArgsConstructor
public class User extends Model<User> {
    // @TableId(type = IdType.AUTO)
    private Long id;
    private String userName;

    @TableField(select = false, fill = FieldFill.INSERT) // 查询时不带这个字段，插入时填充
    private String password;

    private String name;
    private Integer age;

    @TableField(value = "email") // 不是驼峰命名或字段对应不上
    private String mail;

    @TableField(exist = false) // 表中不存在的字段
    private String address;

    // 乐观锁
    @Version
    private Integer version;

    // 逻辑删除字段
    @TableLogic
    private Integer deleted;

    private SexEnum sex;

}