package com.tcwgq.learnmybatisplusspring.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tcwgq.learnmybatisplusspring.model.User;

import java.util.List;

/**
 * @author tcwgq
 * @since 2023/7/9 21:50
 */
public interface UserMapper extends BaseMapper<User> {
    List<User> findAll();

}