package com.tcwgq.learnmybatisplusgenerator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LearnMybatisPlusGeneratorApplication {

    public static void main(String[] args) {
        SpringApplication.run(LearnMybatisPlusGeneratorApplication.class, args);
    }

}
