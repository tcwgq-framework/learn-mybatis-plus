package com.tcwgq.learnmybatisplusgenerator.user.service;

import com.tcwgq.learnmybatisplusgenerator.user.entity.TbUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author itcast
 * @since 2023-07-16
 */
public interface ITbUserService extends IService<TbUser> {

}
