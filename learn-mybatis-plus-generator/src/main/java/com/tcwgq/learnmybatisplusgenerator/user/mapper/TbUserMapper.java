package com.tcwgq.learnmybatisplusgenerator.user.mapper;

import com.tcwgq.learnmybatisplusgenerator.user.entity.TbUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author itcast
 * @since 2023-07-16
 */
public interface TbUserMapper extends BaseMapper<TbUser> {

}
