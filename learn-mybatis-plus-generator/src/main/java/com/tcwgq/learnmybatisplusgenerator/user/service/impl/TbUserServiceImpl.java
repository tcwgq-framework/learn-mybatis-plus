package com.tcwgq.learnmybatisplusgenerator.user.service.impl;

import com.tcwgq.learnmybatisplusgenerator.user.entity.TbUser;
import com.tcwgq.learnmybatisplusgenerator.user.mapper.TbUserMapper;
import com.tcwgq.learnmybatisplusgenerator.user.service.ITbUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author itcast
 * @since 2023-07-16
 */
@Service
public class TbUserServiceImpl extends ServiceImpl<TbUserMapper, TbUser> implements ITbUserService {

}
