package com.tcwgq.learnmybatisplusspringbootoracle.config;

import com.baomidou.mybatisplus.extension.incrementer.OracleKeyGenerator;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * PaginationInterceptor在3.4版本后过时，使用MybatisPlusInterceptor
 *
 * @author tcwgq
 * @since 2023/7/15 21:56
 */
@MapperScan("com.tcwgq.learnmybatisplusspringbootoracle.mapper")
@Configuration
public class MybatisPlusConfiguration {
    @Bean
    public PaginationInterceptor PaginationInterceptor() {
        return new PaginationInterceptor();
    }

    /**
     * 序列生成器
     */
    @Bean
    public OracleKeyGenerator oracleKeyGenerator() {
        return new OracleKeyGenerator();
    }

}
