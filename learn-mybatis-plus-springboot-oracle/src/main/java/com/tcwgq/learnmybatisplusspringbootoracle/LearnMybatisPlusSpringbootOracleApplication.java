package com.tcwgq.learnmybatisplusspringbootoracle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LearnMybatisPlusSpringbootOracleApplication {

    public static void main(String[] args) {
        SpringApplication.run(LearnMybatisPlusSpringbootOracleApplication.class, args);
    }

}
