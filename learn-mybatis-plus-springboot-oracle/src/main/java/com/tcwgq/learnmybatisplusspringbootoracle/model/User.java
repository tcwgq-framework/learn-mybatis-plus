package com.tcwgq.learnmybatisplusspringbootoracle.model;

import com.baomidou.mybatisplus.annotation.KeySequence;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author tcwgq
 * @since 2023/7/9 21:50
 */
@Data
// @TableName("tb_user")
@NoArgsConstructor
@AllArgsConstructor
@KeySequence(value = "SEQ_USER", clazz = Long.class)
public class User {
    // @TableId(type = IdType.AUTO)
    private Long id;
    private String userName;

    @TableField(select = false) // 查询时不带这个字段
    private String password;

    private String name;
    private Integer age;

    @TableField(value = "email") // 不是驼峰命名或字段对应不上
    private String mail;

    @TableField(exist = false) // 表中不存在的字段
    private String address;

}