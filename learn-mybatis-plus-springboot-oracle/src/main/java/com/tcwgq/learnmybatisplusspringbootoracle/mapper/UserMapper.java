package com.tcwgq.learnmybatisplusspringbootoracle.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tcwgq.learnmybatisplusspringbootoracle.model.User;

/**
 * @author tcwgq
 * @since 2023/7/9 21:50
 */
public interface UserMapper extends BaseMapper<User> {
    User findById(Long id);

}