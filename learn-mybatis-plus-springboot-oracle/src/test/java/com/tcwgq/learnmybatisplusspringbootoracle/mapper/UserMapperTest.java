package com.tcwgq.learnmybatisplusspringbootoracle.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tcwgq.learnmybatisplusspringbootoracle.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author tcwgq
 * @since 2023/7/11 21:57
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class UserMapperTest {
    @Resource
    private UserMapper userMapper;

    @Test
    public void insert() {
        User user = new User();
        // user.setId();
        user.setUserName("aaa");
        user.setPassword("bbb");
        user.setName("aaa");
        user.setAge(27);
        user.setMail("aaa@163.com");
        int row = userMapper.insert(user);
        System.out.println("插入用户结果row=" + row);
    }

    @Test
    public void selectById() {
        User user = userMapper.selectById(3L);
        System.out.println(user);
    }

    @Test
    public void selectList() {
        // 查出结果多条报错
        User user = new User();
        user.setUserName("aaa1");
        user.setPassword("bbb1");
        QueryWrapper<User> wrapper = new QueryWrapper<>(user);
        wrapper.like("email", "aaa");
        List<User> users = userMapper.selectList(wrapper);
        for (User user1 : users) {
            System.out.println(user1);
        }
    }

}